/**
* @file   project.cpp
* @author ���������� �.�., ������ 515�
* @date   25 ���� 2018
* @brief  ������ ��������
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <locale.h>
#include <malloc.h>
#include <math.h>
#define Max 6
#define mode 100
/**
* ������� ������� �� ������� number �� m.
*
* @param ����� � 10-�� �.�. number.
* @return ������(void).
*/
void mod(int number);
/**
* ��������� ����� �� ss-�� ������� ��������� � 10-��.
*
* @param *a ��������� �� ������ ������� �������, ss ������� ��������� �����, n ���������� ���� � �����.
* @return ������(void).
*/
void perevod(int *a, int ss, int n);
/**
* ���������� ������ � n-�� ����������� ���� � ss ������� ���������.
*
* @param *a ��������� �� ������ ������� �������, n ���������� ���� � �����, ss ������� ��������� �����.
* @return ������(void).
*/
void gener(int *a, int n, int ss);
int main()
{
	setlocale(LC_ALL, "Rus");
	srand(time(NULL));
	printf("|-----------------------|\n");
	printf("|\t  �������:      |\n");
	printf("|-----------------------|\n");
	printf("|'A' - 10\t'N' - 23|\n|'B' - 11\t'O' - 24|\n|'C' - 12\t'P' - 25|\n|'D' - 13\t'Q' - 26|\n|'E' - 14\t'R' - 27|\n|'F' - 15\t'S' - 28|\n|'G' - 16\t'T' - 29|\n|'H' - 17\t'U' - 30|\n|'I' - 18\t'V' - 31|\n|'J' - 19\t'W' - 32|\n|'K' - 20\t'X' - 33|\n|'L' - 21\t'Y' - 34|\n|'M' - 22\t'Z' - 35|\n");
	printf("|-----------------------|\n");
	int *a;
	int n = rand() % Max + 1;
	a = (int*)malloc(n * sizeof(int));
	int ss = rand() % 35 + 2;
	gener(a, n, ss);
	perevod(a, ss, n);
	printf("\n");
	return 0;
}
void gener(int *a, int n, int ss)
{
	char const mass[37] = { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z' };
	printf("����� � %i ������� ���������: ", ss);
l:
	for (int i = 0; i < n; i++)
	{
		a[i] = rand() % ss;
		if (a[0] == 0)
		{
			goto l;
		}
		printf("%c", mass[a[i]]);
	}
	printf("\n");
}
void perevod(int *a, int ss, int n)
{
	unsigned long int number = 0;
	int g = n;
	for (int i = 0; i < n; i++)
	{
		number = number + a[i] * pow(ss, g - 1);
		g--;
	}
	if (ss == 10)
		;
	else
		printf("����� � 10-�� ������� ���������: %i\n", number);
	mod(number);
}
void mod(int number)
{
	int res;
	int m = rand() % mode + 1;
	res = number % m;
	printf("%i mod %i = %i", number, m, res);
}